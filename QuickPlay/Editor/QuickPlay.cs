using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

namespace QuickPlay {
	public class QuickPlay : EditorWindow {

		public const string CompileMessage 	= "Compiling";
		public const string PauseMessage		= "Paused";
		public const string PlayMessage			= "Playing";
		public const string TransitionMessage	= "Transitioning";
		public const string UnbuiltSceneMessag	= "Scene does not exist in build list.\nAdd it in build settings.";

		List<string> scenePaths = new List<string>();
		List<string> sceneNames = new List<string>();
		int selectedScene = 0;
		string lastScene = "";

		bool startedPlaying = false;
		bool quickPlayed	= false;

		bool displayControls = false;
		string displayMessage = "";

		// Add menu named "Quick Play" to the Window menu
		[MenuItem ("Window/Quick Play")]
		static void Init () {
			// Get existing open window or if none, make a new one:
			QuickPlay window = (QuickPlay)EditorWindow.GetWindow(typeof(QuickPlay));
		}
		
		void Awake() {
			UpdateScenes();
		}
		
		void OnInspectorUpdate() {
			title = "Quick Play";

			if (startedPlaying && !EditorApplication.isCompiling) {
				startedPlaying = false;
				
				EditorApplication.isPlaying = true;
				quickPlayed = true;
				
				Debug.Log("Now playing "+scenePaths[selectedScene]);
			}
			if (quickPlayed && !EditorApplication.isPlaying && !EditorApplication.isUpdating && !EditorApplication.isPlayingOrWillChangePlaymode && !EditorApplication.isPaused && EditorApplication.currentScene == scenePaths[selectedScene]) {
				EditorApplication.OpenScene(lastScene);
				quickPlayed = false;
			}

			bool lastDisplayControls = displayControls;
			string lastDisplayMessage = displayMessage;

			if (EditorApplication.isCompiling) {
				displayControls = false;
				displayMessage = CompileMessage;
			} else if (EditorApplication.isPaused) {
				displayControls = false;
				displayMessage = PauseMessage;
			} else if (EditorApplication.isPlaying) {
				displayControls = false;
				displayMessage = PlayMessage;
			} else if (EditorApplication.isPlayingOrWillChangePlaymode) {
				displayControls = false;
				displayMessage = TransitionMessage;
			} else if (!scenePaths.Contains(EditorApplication.currentScene)) {
				displayControls = false;
				displayMessage = UnbuiltSceneMessage;
				UpdateScenes();
			} else {
				displayMessage = "";
				displayControls = true;
			}

			
			if (displayControls != lastDisplayControls || displayMessage != lastDisplayMessage) {
				Repaint();
			}
		}

		/// <summary>
		/// Gets the list of built scenes, and updates the local list of scene paths and names.
		/// </summary>
		void UpdateScenes() {
			sceneNames.Clear();
			foreach (EditorBuildSettingsScene currScene in EditorBuildSettings.scenes) {
				if (currScene.enabled) {
					scenePaths.Add(currScene.path);
					string name = currScene.path.Substring(currScene.path.LastIndexOf('/')+1);
					name = name.Substring(0,name.Length-6);
					sceneNames.Add(name);
				}
			}
			lastScene = EditorApplication.currentScene;
		}

		void OnGUI () {

			if (displayControls) {
				GUI.enabled = !startedPlaying;

				if (GUILayout.Button("Save and Play Starting Scene")) {
					SaveAndPlayButton();
				}

				GUI.enabled = EditorApplication.isPlaying;
				GUI.enabled = !startedPlaying;

				GUI.enabled = !EditorApplication.isPlaying && !EditorApplication.isPaused;

				if (GUILayout.Button("Refresh built scenes")) {
					UpdateScenes();
				}

				GUILayout.Label ("Starting Scene", EditorStyles.boldLabel);
				selectedScene = EditorGUILayout.Popup(selectedScene, sceneNames.ToArray());

				GUILayout.Label ("Save and Edit Scene", EditorStyles.boldLabel);
				for (int i = 0; i < sceneNames.Count; i++) {
					if (GUILayout.Button(sceneNames[i])) {
						EditorApplication.SaveScene();
						lastScene = scenePaths[i];
						Edit();
					}
				}
			} else {
				GUILayout.Label (displayMessage, EditorStyles.boldLabel);
			}
		}
		
		/// <summary>
		/// Logic for stoping and starting play.
		/// </summary>
		void SaveAndPlayButton() {
			if (EditorApplication.isPlaying) {
				Stop();
			} else {
				Play();
			}
		}

		/// <summary>
		/// Logic for playing from the selected scene.
		/// </summary>
		void Play() {
			lastScene = EditorApplication.currentScene;
			EditorApplication.SaveScene();
			
			if (!EditorApplication.currentScene.Equals(scenePaths[selectedScene])) {
				EditorApplication.OpenScene(scenePaths[selectedScene]);
				Debug.Log("switching to "+scenePaths[selectedScene]);
			}
			startedPlaying = true;
		}
		
		/// <summary>
		/// Logic for stopping and returning to the last edited scene.
		/// </summary>
		void Stop() {
			EditorApplication.isPlaying = false;
			quickPlayed = false;
			if (!EditorApplication.currentScene.Equals(lastScene)) {
				EditorApplication.OpenScene(lastScene);
				Debug.Log("Now editing "+lastScene);
			}
		}

		/// <summary>
		/// Logic for switching to edit another scene.
		/// </summary>
		void Edit() {
			EditorApplication.isPlaying = false;
			quickPlayed = false;

			if (EditorApplication.currentScene != lastScene) {
				EditorApplication.OpenScene(lastScene);
				Debug.Log("Now editing "+lastScene);
			}
		}
	}
}
