QuickPlay
By Brian Ostrander

This is a plugin for Unity, allowing you to easily switch between scenes, and play the game from an arbitrary starting scene. Scene must be added in the build settings.